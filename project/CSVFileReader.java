package project;

import java.util.Scanner;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

public class CSVFileReader {
	Scanner scan;
	BufferedWriter bw = null;
	FileWriter fw = null;
	String content = new String();

	public DataFrame readCSV (File csvFile) throws Exception {
		scan = new Scanner(csvFile);
		DataFrame table = null;
		try {
			if (scan.hasNextLine()) {
				String col = scan.nextLine();
				String[] arr = col.split(",");
				table = new DataFrame (arr);
				while (scan.hasNextLine()) {
					try {
						String rows = scan.nextLine();
						String[] row = rows.split(",");
						table.appendRow(row);
					}
					catch (IOException e) {
						System.out.println("Data format error: " + e.toString());
					}
				}
				return table;
			}
			else {
				System.out.println("File does not exist!");
			}
		}
		catch (IOException e) {
			System.out.println("Error reading text.txt: " + e.toString());
		}
		return table;
	}

	public void writeToCSV (DataFrame f, File csvFile) throws Exception {
		try {

			fw = new FileWriter(csvFile);
			bw = new BufferedWriter(fw);
		}
		catch(FileAlreadyExistsException e) {
			System.out.println("File already exists: " + e.toString());
		}
		try {
			for (int i = 0; i < f.getColumnNames().length; i++) {
				content += f.getColumnNames()[i] + ",";
			}
		}
		catch(Exception e1) {
			System.out.println("Column does not have the correct number of values " + e1.toString());
		}
		try {
			content=content.substring(0, content.length() - 1) + "\r\n";
		}
		catch(Exception e2) {
			System.out.println("Could not return content.length substring: " + e2.toString());
		}
		try {
			for (int i = 0; i < f.getNumRows(); i++) {
				for (int j = 0; j < f.getRow(i).length; j++) {
					content += f.getRow(i)[j] + ",";
				}
				content = content.substring(0, content.length() - 1) + "\r\n";
			}
		}
		catch(Exception e3) {
			System.out.println("Row does not have the correct number of values " + e3.toString());
		}
		try {
			System.out.print(content);
			bw.write(content);
			bw.flush();
			bw.close();
		}
		catch(Exception e4) {
			System.out.println(e4.toString());
		}
	}
}
