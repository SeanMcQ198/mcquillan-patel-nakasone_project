package project;

import java.util.ArrayList;

public class DataFrame {
	private String[] columnNames;
	private ArrayList<String[]> table = new ArrayList<String[]>();

	public DataFrame (String[] columnNames) {
		this.columnNames = columnNames;
	}
	
	

	public int getNumRows() {
		return table.size();
	}
	
	public ArrayList<String[]> getTable(){
		return this.table;
	}

	public String[] getRow (int pos) throws Exception {
		if (pos >= table.size()) {
			throw new Exception("Invalid row");
		}
		else if (pos < 0) {
			throw new Exception("Invalid row");
		}
		return table.get(pos);
	}

	public String getCell (int row, int col) throws Exception {
		if (row > table.size() || col > this.columnNames.length) {
			throw new Exception("Invalid cell");
		}
		return getRow(row)[col];
	}

	public void setCell (int row, int col, String value) throws Exception {
		if (row > table.size() || col > this.columnNames.length) {
			throw new Exception("Invalid cell");
		}
		value = getRow(row)[col];
	}

	public void appendRow (String[] row) throws Exception {
		try {
			table.add(row);
		}
		catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	public String[] getColumnNames() {
		return columnNames;
	}

	public String getColumnName (int pos) throws Exception {
		if (pos > columnNames.length) {
			throw new Exception ("Invalid column");
		}
		else if (pos < 0) {
			throw new Exception ("Invalid column");
		}
		return columnNames[pos];
	}

	public int getColumnPos (String name) throws Exception {
		int pos = 0;
		try {
			for (int i = 0; i < columnNames.length; i++) {
				if (columnNames[i].equals(name)) {
					pos = i;
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.toString());
		}
		return pos;
	}
	
	public void appendData(DataFrame d) throws Exception {
		for(int i = 0; i< d.getNumRows(); i++) {
			this.appendRow(d.getRow(i));}
	}
	
	public String toString() {
				StringBuilder turner = new StringBuilder();
				int count=0;
				int county=0;
				for(String s: this.columnNames) {
					count++;
					if(this.columnNames.length == count)
						turner.append(s);
					else
					turner.append(s+ ", ");
					}
				turner.append("\n");
				for(String[] arr: this.table) {
					for(String s: arr)
						if(arr.length == county)
							turner.append(s);
						else
						turner.append(s+ ", ");
					turner.append("\n");
					county++;}
				return turner.toString();
				}
	public static void main(String[] args) throws Exception {
		String[] collums = {"name", "shoe size", "birth date", "death date"};
		String[] checkCollums = {"name", "shoe size birth date", "death date"};
		DataFrame that = new DataFrame(collums);
		DataFrame those = new DataFrame(checkCollums);
		that.appendRow(new String[] {"Justin Schmitz", "12.2", "05/12/00", "12/25/80"});
		that.appendRow(new String[] {"Joston Schmotz", "3", "05/12/00", "12/25/30"});
		that.appendRow(new String[] {"Jesten Schmetz", "15", "05/12/00", "12/25/80"});
		those.appendRow(new String[] {"Justin Schmitz", "12.2", "05/12/00", "12/25/80"});
		those.appendRow(new String[] {"Jesten Schmetz", "15", "05/12/00", "12/25/80"});
		those.appendRow(new String[] {"Joston Schmotz", "3", "05/12/00", "12/25/30"});
		those.appendRow(new String[] {"Jesten Schmetz", "15", "05/12/00", "12/25/80"});
		those.appendRow(new String[] {"Jesten Schmetz", "15", "05/12/00", "12/25/80"});
		that.appendData(those);
		System.out.println(that.toString());
	}
	/*public String toString() {
        StringBuilder b = new StringBuilder();

        // append column names
        serializeRow(b, columnNames);

        // append horizontal divider
        for(int i = 0; i < columnNames.length; i++) {
            for(int j = 0; j < columnWidths[i] + 3; j++)
                b.append('-');
        }
        b.append('\n');

        // print data
        for(String[] row: rows) {
            serializeRow(b,row);
        }

        return b.toString();
    }
	
	private void serializeRow(StringBuilder b, String[] row) {
        for(int i = 0; i < columnNames.length; i++) {
            pad(b, row[i], columnWidths[i]);
            if (i != columnNames.length - 1) 
                b.append("|");
        }
        b.append('\n');
    }*/
}