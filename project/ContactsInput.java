package project;
import java.util.Arrays;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class ContactsInput {
	public static void main(String[] args) throws Exception{
	    Scanner scan = new Scanner(System.in);
	    String[] collums = {"Name", "address", "email", "phone number"};
	    DataFrame frame = new DataFrame(collums);
	    ContactsList contacts = new ContactsList(frame);
	    CSVFileReader reader = new CSVFileReader();
	    boolean flag = true;
	    while(flag){
	      System.out.println("Enter \"1\" to import contacts from a file"
	      + " \nEnter \"2\" to input contacts individually \nEnter \"3\" to write contacts to a file"
	      + " \nEnter \"4\" to search for a contact \nEnter \"5\" to delete a contact");
	      int input = scan.nextInt();
	      switch (input){
	        case  1: {
	        	  String theFile;
		          File f;
		          boolean guy = false;
	        	do {
	          System.out.println("Enter file name. Include \".csv\"");
	          theFile = scan.next();
	          f = new File(theFile);
	          try {
	          DataFrame d = reader.readCSV(f);
	         // System.out.println(d.toString());
	          contacts.getDataFrame().appendData(d);
	          //System.out.println(contacts.getDataFrame().getTable().size());
	          }
	          catch (IOException e) {
	        	  System.out.println("File does not exist or cannot be read");
	        	  System.out.println("would you like to try again? Enter \"true\" for yes or \"false\" for no");
	        	  guy = scan.nextBoolean();
	          }
	          } while(guy);
	        }
	        break;
	        
	        case 2: {
	        	String[] newCon = new String[4];
	        	//String fullName;
	        	//String fullAddress;
	        	boolean flog = true;
	        	while(flog) {
	        		System.out.println("Enter contact name");
	        		scan.nextLine();
	        		String fullName = scan.nextLine();
	        		newCon[0] = fullName;
	        		System.out.println("Enter contact address");
	        		String fullAddress = scan.nextLine();
	        		newCon[1] = fullAddress;
	        		System.out.println("Enter contact email");
	        		newCon[2] = scan.next();
	        		System.out.println("Enter contact phone number");
	        		newCon[3] = scan.next();
	        		contacts.addContact(newCon);
	        		//System.out.println(Arrays.toString(newCon));
	        		System.out.println("Would you like to enter another contact? Enter \"yes\" or \"no\"");
	        		String key = scan.next();
	        		if (key.equalsIgnoreCase("no"))
	        			flog = false; 
	        	}
	        	break;
	        }
	        
	        case 3: {
	        	String theFile;
		          File f;
		          boolean guy = false;
	        	do {
	          System.out.println("Enter file name. Include \".csv\"");
	          theFile = scan.next();
	          f = new File(theFile);
	          try {
	          reader.writeToCSV(contacts.getDataFrame(),f);
	          }
	          catch (IOException e) {
	        	  System.out.println("File does not exist or cannot be read");
	        	  System.out.println("would you like to try again? Enter \"true\" for yes or \"false\" for no");
	        	  guy = scan.nextBoolean();
	          }
	          } while(guy);
	        }
	        break;
	        
	        case 4: {
	        	String[] back;
	        	String guy = "";
	        	do {
	        	System.out.println("Enter search name");
	        	scan.nextLine();
	        	String search = scan.nextLine();
	        	back = contacts.searchContact(search);
	        	if(back == null) {
	        		System.out.println("no such contact.\nWould you like to try again? "
	        				+"Enter yes or no");
	        		guy = scan.next();
	        		} } while(guy.equalsIgnoreCase("yes"));
	        	
	        	if(!(back == null))
	        	System.out.println(Arrays.toString(back));
	        }
	        break;
	        
	        case 5: {
	        	boolean that;
	        	String gey ="";
	        	do {
	        	System.out.println("Enter name of contact to delete");
	        	scan.nextLine();
	        	String bye = scan.nextLine();
	        	that = contacts.deleteContact(bye);
	        	if(that)
	        		System.out.println("contact deleted");
	        	else { System.out.println("no such contact.\nWould you like to try again? "
        				+"Enter yes or no");
	        			gey = scan.next();
	        			}
	        	} while (gey.equalsIgnoreCase("yes"));
	        }
	        break;
	      }
	      String in;
	      boolean innerFlag;
	      
	      do {
	      System.out.println("Would you like to perform another action? Enter \"yes\" or \"no\"");
	      in = scan.next();
	      if(in.equalsIgnoreCase("no")) {
	    	  flag = false;
	    	  innerFlag = false;
	    	  System.out.println("Program closing");
	    	  scan.close();
	    	  System.exit(0);
	    	  }
	      else if(in.equalsIgnoreCase("yes"))
	    	  innerFlag = false;
	      else innerFlag = true;
	      } while (innerFlag);
	    }
	  }
	}