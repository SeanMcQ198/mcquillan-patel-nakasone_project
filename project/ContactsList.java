package project;

import java.io.File;
import java.util.Arrays;

public class ContactsList{
	private DataFrame conList;

	public ContactsList(DataFrame data){
		this.conList = data;
	}
	
	public DataFrame getDataFrame() {
		return this.conList;
	}

	public void addContact(String[] con) throws Exception {
		conList.appendRow(con);
	}

	public void addContact(DataFrame d) throws Exception {
		this.conList.appendData(d);
	}

	public boolean deleteContact(String con) throws Exception {
		String[] found = this.searchContact(con);
		//System.out.println(Arrays.toString(found));
		 if(found != null){
			conList.getTable().remove(found);
			return true;}
			else return false;
	}

	public void readingCSV(File f) throws Exception {
		CSVFileReader read =  new CSVFileReader();
		DataFrame d = new DataFrame(conList.getColumnNames());
		//this.conList = read.readCSV(f);
		d = read.readCSV(f);
		this.conList.appendData(d);
	}
	
	public String[] searchContact(String name) throws Exception {
		boolean flag = false;
		String[] found = new String[conList.getColumnNames().length];
		for (int i = 0; i < conList.getTable().size(); i++) {
			if(conList.getTable().get(i)[0].contains(name)) {
				found = conList.getTable().get(i);
				flag = true;
			}
		}
		if(flag)
			return found;
		else return null;
	}



	public static void main(String[] args) throws Exception{
		String[] columns = {"Name", "Address", "email", "Phone Number"};
		File f = new File("contact.csv");
		DataFrame contacts = new DataFrame(columns);
		ContactsList c = new ContactsList(contacts);
		c.addContact(new String[] {"Sean McQuillan", "886 Lincoln Ave", "seanmcquillan@hawk.iit.edu", "6128400813"});
		c.addContact(new String[] {"Pratik Patel", "4828 Avers Ave", "ppatel127@hawk.iit.edu", "7739835002"});
		c.readingCSV(f);
		for(String[] s: c.getDataFrame().getTable())
			System.out.println(Arrays.toString(s));
		/*System.out.println(Arrays.toString(c.searchContact("Sean McQuillan")));
		System.out.println(c.searchContact("guy"));
		System.out.println(c.deleteContact("McQuillan"));
		System.out.println(Arrays.toString(c.searchContact("Sean McQuillan")));
		
		//File cons = new File("/Users/pratikpatel/eclipse-workspace/cs116-s19-ppatel127/src/lecture.project/contact.csv");
		//c.readCSV(cons);*/
		
	}
}
